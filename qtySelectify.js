$.fn.qtySelectify = function(options) {
  // Default options
  var defaults = {
    maxOptions: 10,
    className: 'selectify',
    allowMore: false,
    copyClasses: false
  };
  // Combine defaults with passed in options
  var opts = $.extend({}, defaults, options);
  // Run on each selected node
  return this.each(function() {

    var $this, // Cache this
        input, // Ref to original qty input
        inputClass, // Holds the class attribute of the original input
        inputVal, // Holds the value of the original input
        select, // The new select we will create
        options, // The options we will create
        numberOfOptions, // The number of options that need to be created
        selectifyContainer; // The container for our new select

    $this = $(this);
    // If this element has already been transformed, exit
    if($this.parent().find('[data-qtySelectify]').length > 0) {
      console.log('qtySelectify has already ran on this element', $this[0]);
      return; //Don't allow it to be run twice on the same input;
    }

    input = $this.is('input') ? $this : $this.find('input').eq(0);
    inputClass = input.attr('class') || '';
    inputVal = parseInt(input.val());
    numberOfOptions = (inputVal > opts.maxOptions) ? inputVal : opts.maxOptions;

    // Hide the original input
    input.attr('style', 'display:none;').addClass(opts.className + '__input');
    // Create our new quantity dropdown and link it's value to the original input
    select = $('<select></select>').addClass(opts.className + '__select').on('change', function() {
      if($(this).val() !== 'more') {
        input.val($(this).val());
      }
    });
    // If both the input and the select should have the same classes, add them
    if(opts.copyClasses) {
      select.addClass(inputClass);
    }
    // Create the options
    options = $.map(new Array(numberOfOptions), function(n, i) {
      var val = i+1;
      var option = $('<option value="' + val + '">' + val + '</option>');
      if(inputVal == val) {
        option.attr('selected', 'selected');
      }
      return option[0]; // Return the dom node not a jQuery object
    });
    // Add them to the select
    select.append(options);
    // If the inputVal is 0 we need to prepend a 0
    if(inputVal === 0)  {
      select.prepend('<option value="0">0</option>');
    }
    // If user should be able to select more than maxOptions add a 'More' option
    // which will swap back to the original input
    if(opts.allowMore) {
      // Append the more option and attach a change handler
      select.append('<option value="more">More</option>').on('change', function(e) {
        var newVal = select.val();
        if(newVal === 'more') {
          select.hide();
          input.show().focus().select();
        } else {
          if(newVal != inputVal) { // Only change if necessary
            input.val(select.val()).change();
          }
        }
      });
    }
    // Add all the elements to the dom
    selectifyContainer = $('<div class="' + opts.className + '"></div>');
    selectifyContainer.append(select);
    input.after(selectifyContainer); // Add the container next to the original
    selectifyContainer.append(input); // Place the original inside the container
    $this.attr('data-qtySelectify', ''); // Add data attribute so this can't run again
  });
}
